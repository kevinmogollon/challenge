#!/bin/bash
export IMAGE_FAMILY="centos-8"
export ZONE="us-central1-a"
export INSTANCE_NAME="task2"
export INSTANCE_TYPE="e2-medium"
#export SERVICE_ACCOUNT="kevinmogollon"
gcloud compute instances create $INSTANCE_NAME \
        --zone=$ZONE \
        --image-family=$IMAGE_FAMILY \
        --maintenance-policy=TERMINATE \
        --machine-type=$INSTANCE_TYPE \
        --boot-disk-size=20GB
