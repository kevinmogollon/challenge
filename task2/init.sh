#!/bin/bash
#export FULL_PATH_CREDENTIALS="/home/digeti/Documentos/challengue-everis/task2/challenge-everis-cdcff67da81b.json"
export FULL_PATH_CREDENTIALS="$(pwd)/keys"
export FULL_PATH_ANSIBLE="$(pwd)/ansible"
export FULL_PATH_JENKINS="$(pwd)/jenkins"
#gcloud config set project $(echo $GOOGLE_PROJECT)

### environment declaration for google instance
export IMAGE_FAMILY="centos-8"
export ZONE="us-central1-a"
export INSTANCE_NAME="task2"
export INSTANCE_TYPE="e2-medium"

### utilities environment declaration
export KEY_RSA_PUB="${INSTANCE_NAME}_rsa.pub"
export KEY_RSA_PRIVATE="${INSTANCE_NAME}_rsa"
export KEY_GOOGLE="${INSTANCE_NAME}.json"

### autenticate with json credentians downloaded previous
gcloud auth activate-service-account --key-file "${FULL_PATH_CREDENTIALS}/${KEY_GOOGLE}"

### creando vm with environments begin declared
gcloud compute instances create $INSTANCE_NAME \
        --zone=$ZONE \
        --image-family=$IMAGE_FAMILY \
        --image-project="centos-cloud" \
        --machine-type=$INSTANCE_TYPE \
        --boot-disk-size=50GB \
        --tags http-server \
        --metadata-from-file ssh-keys="${FULL_PATH_CREDENTIALS}/${KEY_RSA_PUB}"

## get ip external google instance after created ###
export IP_EXTERNAL=$(gcloud compute instances describe ${INSTANCE_NAME} --zone=${ZONE} --format='get(networkInterfaces[0].accessConfigs[0].natIP)')
echo -e "[vm]\ntask2    ansible_host=${IP_EXTERNAL} ansible_ssh_extra_args='-o StrictHostKeyChecking=no' ansible_user=${USER} ansible_port=22 ansible_ssh_private_key_file=${FULL_PATH_CREDENTIALS}/${KEY_RSA_PRIVATE}" > "${FULL_PATH_ANSIBLE}/inventory/${INSTANCE_NAME}.ini"
echo -e "JENKINS_ADMIN_ID=admin\nJENKINS_ADMIN_PASSWORD=12345678\nSERVER_IP=${IP_EXTERNAL}"> "${FULL_PATH_JENKINS}/.env"
scp -i "${FULL_PATH_CREDENTIALS}/${KEY_RSA_PRIVATE}" -o StrictHostKeyChecking=no -r jenkins ansible/task "${USER}@${IP_EXTERNAL}":~
#scp -i "${FULL_PATH_CREDENTIALS}/${KEY_RSA_PRIVATE}" -r jenkins ansible/task "${USER}@${IP_EXTERNAL}":~
ansible-playbook "${FULL_PATH_ANSIBLE}/playbooks/task2.yml" -i "${FULL_PATH_ANSIBLE}/inventory/task2.ini"
gcloud compute firewall-rules update default-allow-http --rules=tcp:8080,tcp:80
###### END CREANDO VM #######
#gcloud compute ssh $INSTANCE_NAME --command="ls"
#ip add
##### ssh connect ########
#gcloud compute ssh challengue-task2  --zone=us-central1-a
#ssh-keygen -t rsa -f ~/Documentos/challengue-everis/challengue-task2_rsa -C $USER
echo -e "Thank you !!!\n Your jenkins server is running ${IP_EXTERNAL}:80 in few minutes"
