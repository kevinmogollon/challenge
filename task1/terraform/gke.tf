variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes"
}

# GKE cluster
resource "google_container_cluster" "primary" {
  name     = "${var.project_id}-gke"
  location = var.region

  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  master_auth {
    username = var.gke_username
    password = var.gke_password

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = "${google_container_cluster.primary.name}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    machine_type = "n1-standard-1"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

resource "kubernetes_deployment" "flask-deployment" {
  metadata {
    name = "flask-deployment"
  }

  spec {
    selector {
      match_labels = {
        app = "flask"
      }
    }

    template {
      metadata {
        labels = {
          app = "flask"
        }
      }

      spec {
        container {
          image = "gcr.io/${var.project_id}/mypythonapp:latest"
          name  = "flask"
          port {
            container_port = 5000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "flask-service" {
  metadata {
    name = "flask-service"
  }
  spec {
    selector = {
      app = "flask"
    }
    port {
      port        = 80
      target_port = 5000
      name = "http"
    }
  }
}

resource "kubernetes_ingress" "flask-ingress" {
  metadata {
    name = "flask-ingress"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx",
      "nginx.ingress.kubernetes.io/backend-protocol" = "HTTP",
      "nginx.ingress.kubernetes.io/ssl-redirect" = false,
      "kubernetes.io/ingress.allow-http" = "yes"
    }
  }

  spec {
    rule {
      http {
        path {
          backend {
            service_name = "flask-service"
            service_port = 80
          }
        }
      }
    }
  }
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}
