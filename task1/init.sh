#!/bin/bash
## CHANGE THIS VARIABLE FOR AFFECT GK NAME
export TF_VAR_project_id="challenge-everis"
export TF_VAR_region="us-central1"
## CHANGE THIS VARIABLE FOR AFFECT GK NAME
export FULL_PATH_CREDENTIALS="$(pwd)/keys"
export FULL_PATH_APP="$(pwd)/mypythonapp"
export FULL_PATH_TERRAFORM="$(pwd)/terraform"
export INSTANCE_NAME="task1"
export KEY_GOOGLE="${INSTANCE_NAME}.json"
export GOOGLE_APPLICATION_CREDENTIALS="${FULL_PATH_CREDENTIALS}/${KEY_GOOGLE}"

if [ "$1" == "CREATE" ]; 
then
    echo "opción selecionada es: $1"
    cd $FULL_PATH_APP && sudo docker build . -t gcr.io/challenge-everis/mypythonapp
    gcloud auth activate-service-account --key-file "${FULL_PATH_CREDENTIALS}/${KEY_GOOGLE}"
    docker push gcr.io/${TF_VAR_project_id}/mypythonapp
    cd $FULL_PATH_TERRAFORM && terraform init && \
   
    terraform apply
    gcloud container clusters get-credentials "${TF_VAR_project_id}-gke" --region $TF_VAR_region
    #gcloud container clusters get-credentials $(terraform output kubernetes_cluster_name) --region $(terraform output region)
    #gcloud container clusters get-credentials "${TF_VAR_project_id}-gke" --region $TF_VAR_region
    #kubectl get ingress flask-ingress --watch
    #kubectl apply -f "${FULL_PATH_TERRAFORM}/mypythonapp.yml"
    #gcloud container clusters get-credentials "${TF_VAR_project_id}-gke" dos-terraform-edu-gke --region us-central1
elif [ "$1" == "DESTROY" ]; then
       echo "opción selecionada es: $1"
       terraform destroy
elif [ "$1" == "OUTPUT" ]; then
        echo "opción selecionada es: $1"
elif [ -z "$1" ]; then
        echo -e "Debe ingresar una de las siguientes opciones:\nCREATE\nDESTROY\nOUTPUT"  
else
     echo -e "Parámetro inválido, debe ingresar una de las siguientes opciones:\nCREATE\nDESTROY\nOUTPUT"    
fi
