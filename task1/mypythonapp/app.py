from flask import Flask, request, jsonify, Response
from array import *
import json
import os

app = Flask(__name__)

@app.route('/greetings', methods=['GET'])
def index():
    return jsonify({'message':  'Hello World from '+os.environ['HOSTNAME'],'status': 202})

@app.route('/square/<X>', methods=['GET'])
def square(X):
    x=int(X)
    square_result= x ** 2
    return jsonify({'message':  'number:' + str(square_result) ,'status': 202})

@app.route('/square', methods=['POST'])
def square_post():
    if not request.json:
        abort(400)
    X=request.json['number']
    Y=request.json['square']
    square_result= (int(X) ** int(Y))
    return jsonify({'message': str(square_result) ,'status': 202})

@app.errorhandler(404)
def not_found(error=None):
    response = jsonify({
        'message':  'Resource Not Found:' + request.url,
        'status': 404
    })
    response.status_code = 404
    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

 
# result = model.predict([28, 16, 30, 28, 26, 36])
# result = modelSVM.predict({'Realista': 28, 'Investigador': 16, 'Artístico': 30, 'Social': 28, 'Emprendedor': 26, 'Convencional': 36})
# resultTree = model.predict(pd.Series({'Realista': 28, 'Investigador': 16, 'Artístico': 30, 'Social': 28, 'Emprendedor': 26, 'Convencional': 36}))